import biopytainer


input = biopytainer.config.load_configfile("config.yaml")

fastqc_out = biopytainer.container.fastqc_v0_11_15.run_parallel(input)

trimmed = biopytainer.container.fastqc_v0_11_15.run_parallel()



